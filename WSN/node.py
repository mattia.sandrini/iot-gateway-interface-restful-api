
class Node():
    
    """
    programmable = None # boolean
    controllable = None # boolean
    bookable = None     # boolean
    node_id = None      # integer
    identifier = ''     # string
    start_time = None   # time without time zone
    stop_time = None    # time without time zone
    indoor = None       # boolean
    operating_mode = '' # string
    label = ''          # string
    """
    
    node_id = None      # integer
    identifier = ''     # string
    indoor = None       # boolean
    label = ''          # string
    
    sensors = []
    actuators = []
    
    unique_identifier = ""
    
    
    def __init__(self):
        pass
    
    
    ## Costruisce l'identificativo univoco del nodo, assemblando l'identificativo del nodo stesso
    #  assieme all'identificativo univoco del locale che lo contiene, e così via.
    #  Poi istruisce anche il resto della catena a fare lo stesso.
    def built_unique_identifiers(self, parent):
        self.unique_identifier = parent +':'+ self.identifier  # Concateno l'identificatore univoco del genitore con l'identificatore dell'oggetto considerato
        for sensor in self.sensors:
            sensor.built_unique_identifiers(self.unique_identifier)
        for actuator in self.actuators:
            actuator.built_unique_identifiers(self.unique_identifier)
    
    
    ##
    #  @return Lista delle variable associate ai sensor contenuti in questo node
    def get_sensor_variables(self):
        result = []
        for sensor in self.sensors:
            result.extend(sensor.variables)
        return result
    
    ##
    #  @return Lista delle variable associate agli actuator contenuti in questo node
    def get_actuator_variables(self):
        result = []
        for actuator in self.actuators:
            result.extend(actuator.variables)
        return result
    
    ##
    #  @return Lista completa delle variable osservate e/o controllate in questo node
    def get_variables(self):
        sensor_varaibles = self.get_sensor_variables()
        actuator_varaibles = self.get_actuator_variables()
        
        return sensor_varaibles + actuator_varaibles
    
    
    ##
    # @param sensor_id
    # @return Il sensor con id pari a quello richiesto; None se non lo trova
    def get_sensor(self, sensor_id):
        for sensor in self.sensors:
            if sensor.sensor_id == sensor_id:
                return sensor
        return None 
    
    ##
    # @param actuator_id
    # @return L'actuator con id pari a quello richiesto; None se non lo trova
    def get_actuator(self, actuator_id):
        for actuator in self.actuators:
            if actuator.actuator_id == actuator_id:
                return actuator
        return None 
    
    ##
    # @param variable_id
    # @return La variable con id pari a quello richiesto; None se non la trova
    def get_variable(self, variable_id):
        for sensor in self.sensors:
            result = sensor.get_variable(variable_id)
            if result != None:                         # Quando la trova
                return result                          # la restituisce
        for actuator in self.actuators:                # Se non l'ha ancora trovata cerca tra gli actutors
            result = actuator.get_variable(variable_id)
            if result != None:                         # Quando la trova
                return result                          # la restituisce
        return None                                    # Se non la trova ancora ritorna None
    
    
    
    
    
    
    