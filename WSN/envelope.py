import xml_serializer, xml_deserializer

## Questa classe involucro contiene la lista di oggetti building, e si occupa di effettuare
#  le ricerche di specifiche building, room, node, sensor, actuator e varaible,sulla base
#  del valore dell'id specificato. Sfrutta 
class Envelope():

    buildings = []
    
    # Vengono segnalati quelli che sono gli identificatori tra gli attributi delle classi qui considerate, 
    # in queto modo è possibile aggiungere, nel file XML, un attributo che riporti l'identificatore dell'oggetto per ogni tag che rappresenti quell'oggetto,
    # questo per poter effettuare un ricerca nel file XML direttamente con i metodi di parsing nativi del python, quando ce ne dovesse essere la necessità.
    identifier_list = ['building_id', 'room_id', 'node_id', 'sensor_id', 'actuator_id', 'variable_id']
    
    
    ## Istanzia un oggetto della classe Envelope contenente l'eventuale lista di buildings specificate
    def __init__(self, buildings=[]):
        self.buildings = buildings
        
    
    ## Serializza tutte le informazioni mantenute nella struttura di contenimento (partendo dalla lista di buildings) in un file XML
    #  @param file_xml Nome del file in cui scrivere le informazioni
    def serialize(self, file_xml):
        xml_serializer.serialize(self.buildings, 'buildings', self.identifier_list, file_xml)
    
    ## Deserializza  le informazioni contenute nel file XML specificato, e le pone nella struttura di contenimento rappresentata dalla lista di buildings
    #  @param file_xml Nome del file da cui leggere le informazioni   
    def deserialize(self, file_xml):
        self.buildings = xml_deserializer.deserialize(file_xml)

    
    ## Questo metodo costruisce per ogni building, room, node, sensor, actuator e variable, un id univoco
    #  semplicemente concatenando gli identificatori alfanumerici dei componenti che costituiscono la catena.
    #  Deve essere richiamato ogni volta che si inserisce un nuovo elemento nella catena, ma si potrebbe scrivere
    #  un metodo simile che vada direttamente a lavorare sul nodo appena aggiunto invece che rifare tutta la procedura.
    def built_unique_identifiers(self):
        for building in self.buildings:
            building.built_unique_identifiers()
        

        
    
    ## Fornisce la lista completa di room presenti nel sistema
    def get_all_rooms(self):
        result = []
        for building in self.buildings:     # Per ogni building
            result.extend(building.rooms)   # Estendo la lista risultante con la lista di room contenute nel building attuale
        return result
    
    ## Fornisce la lista completa di node presenti nel sistema
    def get_all_nodes(self):
        result = []
        for room in self.get_all_rooms():   # Per ogni stanza di ogni building
            result.extend(room.nodes)       # Estendo la lista risultante con la lista di room contenute nel building attuale
        return result
        
    ## Fornisce la lista completa di sensor presenti nel sistema
    def get_all_sensors(self):
        result = []
        for node in self.get_all_nodes():      # Per ogni stanza di ogni building
            result.extend(node.sensors)   # Estendo la lista risultante con la lista di room contenute nel building attuale
        return result
        
    ## Fornisce la lista completa di actuator presenti nel sistema   
    def get_all_actuators(self):
        result = []
        for node in self.get_all_nodes():      # Per ogni stanza di ogni building
            result.extend(node.actuators) # Estendo la lista risultante con la lista di room contenute nel building attuale
        return result
        
    ## Fornisce la lista completa di variable presenti nel sistema   
    def get_all_variables(self):
        result = []
        for sensor in self.get_all_sensors():      # Per ogni stanza di ogni building
            result.extend(sensor.variables)   # Estendo la lista risultante con la lista di room contenute nel building attuale
        for actuator in self.get_all_actuators():  # Per ogni stanza di ogni building
            result.extend(actuator.variables) # Estendo la lista risultante con la lista di room contenute nel building attuale
        # Operando in questo modo risultano prima tutte le variabili associata a i sensori e poi quelle degli attuatori
        return result 
        
    
    ## Effettua la ricerca su tutti i building in base all'identificatore specifiato
    #  @param building_id
    #  @return Istanza delle classe Building con building_id pari a quello specificato, None se la ricerca non da risultato
    def search_building(self, building_id):
        for building in self.buildings:
            if building.building_id == int(building_id): # E' indispensabile avere un valore intero!
                return building
        return None
    
    ## Effettua la ricerca su tutte le room presenti nel sistema, in base all'identificatore specifiato
    #  @param room_id
    #  @return Istanza delle classe Room con room_id pari a quello specificato, None se la ricerca non da risultato
    def search_room(self, room_id):
        room_id = int(room_id)   # Converto da stringa a intero
        for building in self.buildings:
            result = building.get_room(room_id)
            if result != None:       # Trovato
                return result
        return None 
    
    ## Effettua la ricerca su tutti i node presenti nel sistema, in base all'identificatore specifiato
    #  @param node_id
    #  @return Istanza delle classe Node con node_id pari a quello specificato, None se la ricerca non da risultato
    def search_node(self, node_id):
        node_id = int(node_id)   # Converto da stringa a intero
        for building in self.buildings:
            result = building.get_node(node_id)
            if result != None:       # Trovato
                return result
        return None 
        
    ## Effettua la ricerca su tutti i sensor presenti nel sistema, in base all'identificatore specifiato
    #  @param sensor_id
    #  @return Istanza delle classe Sensor con sensor_id pari a quello specificato, None se la ricerca non da risultato
    def search_sensor(self, sensor_id):
        sensor_id = int(sensor_id)   # Converto da stringa a intero
        for building in self.buildings:
            result = building.get_sensor(sensor_id)
            if result != None:       # Trovato
                return result
        return None 
    
    ## Effettua la ricerca su tutti gli actuator presenti nel sistema, in base all'identificatore specifiato
    #  @param actuator_id
    #  @return Istanza delle classe Actuator con actuator_id pari a quello specificato, None se la ricerca non da risultato
    def search_actuator(self, actuator_id):
        actuator_id = int(actuator_id)   # Converto da stringa a intero
        for building in self.buildings:
            result = building.get_actuator(actuator_id)
            if result != None:           # Trovato
                return result
        return None 
    
    ## Effettua la ricerca su tutte le variable presenti nel sistema, in base all'identificatore specifiato
    #  @param variable_id
    #  @return Istanza delle classe Variable con variable_id pari a quello specificato, None se la ricerca non da risultato   
    def search_variable(self, variable_id):
        variable_id = int(variable_id)   # Converto da stringa a intero
        for building in self.buildings:
            result = building.get_variable(variable_id)
            if result != None:           # Trovato
                return result
        return None 
    
    
    
    
    

