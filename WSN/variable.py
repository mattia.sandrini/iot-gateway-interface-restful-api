
import influx
import domoticz

## Questa classe utilizza le funzioni esposte da influx e domoticz per accedere al database ed effetture i comandi di controllo.
#  Deve conoscere anche la struttura delle tabelle di influxdb (nome delle measurement, nome dei tag, ecc...)
#
class Variable():
    
    """
    label = ''                  # string
    variableeid = ''            # string
    accuracy = None             # float
    variableid = None           # integer
    name = ''                   # string
    description = ''            # string
    measure = ''                # string
    responsetime = None         # float
    operatingmaxvalue = None    # integer
    operatingminvalue = None    # integer
    """
    
    measurement = ''    # string # Nome della measurement, in cui ogni valore è discriminato da tag come variable_id, ecc
    variable_id = None  # integer
    name = ''           # string
    description = ''    # string
    unit = ''           # string # Unità di misura
    accuracy = None     # float
    controllable = None # boolean  # Indica se appartiene ad un attuatore o meno, cioè se può essere impostata dall'esterno (p.e. On/Off di uno switch)
    domoticz_device_idx = None  # integer  # Identificativo del device registrato in domoticz, ache corrisponde al sensore/attuatore a cui affereisce questa variable
    
    identifier = ''
    unique_identifier = ''
    
    
    def __init__(self):
        pass
    
    
    ## Costruisce l'identificativo univoco della variabile, assemblano l'identificativo della variabile stessa
    #  assieme all'identificativo univoco del sensore/attuatore che la contiene, e così via...
    def built_unique_identifiers(self, parent):
        self.unique_identifier = parent +':'+ self.identifier
    
    
    ## 
    #  @param from_ Timestamp che segna l'inizio dell'intervallo temporale da considerare (opzionale)
    #  @param to    Timestamp che segna la fine  dell'intervallo temporale da considerare (opzionale)
    #  @return Lista dei valori assunti dalla variabile considerata, all'interno di un eventuale intervallo temporale specificato
    def get_values(self, from_='', to=''):
        return influx.get_measurement_by_interval(self.measurement, { 'variable_id': self.variable_id }, from_, to)
        
    
    
    # NB: Interrogare il database per ottenere l'ultimo valore registrato potrebbe risultare costoso se sono presenti una grande quantità di misure nella stessa measurement
    
    ## 
    #  @return Ultimo valore assunto dalla variabile considerata
    def get_last_value(self):
        # Per ottenere l'ultimo valore assunto dalla variable considerata interrogo il database 
        # e richiedo l'ultimo point registrato nella measurement di questa variable, selezionando in base
        # al tag variable_id specificandovi quello proprio della variable considerata
        return influx.get_last_measure(self.measurement, { 'variable_id': self.variable_id })
    
    
    """ Alternativa per ottenere l'ultima misura senza interrogare InfluxDB
    ## 
    #  @return Ultimo valore assunto dalla variabile considerata
    def get_last_value(self):
        # Per ottenere l'ultimo valore assunto dalla variable considerata interrogo le Domoticz API
        return domoticz.get_device_values(self.domoticz_device_idx)
    """
    
    
    ## Se la variabile è controllabile (cioè appartiene ad un attuatore), prova ad eseguire il comando attraverso domoticz,
    #  @param new_value Il nuovo valore che assume la variabile (stato di uno switch, ...)
    #  @return True se ha avuto successo, altrimenti False
    def set_value(self, new_value):
        if self.controllable:        # Se consente di applicare un'azione all'attuatore a cui appartiene
            # Eseguo il comando attraverso domoticz, e valuto se è andato a buon fine
            success = domoticz.command_switch_light(idx=self.domoticz_device_idx, new_status=new_value)
            if not success:
                pass        # Dovrei fare il log, lanciare un'eccezione o altro
            return success
        else:
            return False
    