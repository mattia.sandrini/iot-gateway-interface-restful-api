

class Building():
    
    """
    building_id = None           # Integer
    identifier = ""              # String
    building_year = None         # Integer
    energy_class = ""            # String
    orientation = ""             # String
    area = None                  # Float
    volume = None                # Float
    covering_type = ""           # String
    avg_hight_ceiling = None     # Float
    num_floors = None            # Integer
    num_rest_rooms = None        # Integer
    num_offices = None           # Integer
    num_conference_rooms = None  # Integer
    num_class_rooms = None       # Integer
    num_rooms = None             # Integer
    address = ""                 # String
    climate_zone = ""            # String
    gazebo = None                # Boolean
    residential = None           # Boolean
    non_residential = None       # Boolean
    label = ""                   # String
    latitude = None              # Float
    longitude = None             # Float
    degree_days = None           # Integer
    """
    
    building_id = None           # Integer
    identifier = ""              # String
    num_rooms = None             # Integer
    
    unique_identifier = ""       # String  (Generato automaticamente)
    
    rooms = []
    
    
    def __init__(self):        
        pass
    
    
    ## Inizializza l'identificativo univoco dell'edificio, semplicemente scrivendovi l'identificativo dell'edificio.
    #  Poi istruisce il resto della catena a costruire il proprio identificatore univoco per ogni oggetto che la costituisce,
    #  semplicemente concatendando i vari identificatori in questo modo: building_identifier:room_identifier:...:variable_identifier
    def built_unique_identifiers(self):
        self.unique_identifier = self.identifier
        for room in self.rooms:
            room.built_unique_identifiers(self.unique_identifier)
                    
                  
    ## 
    # @return Lista completa di node presenti in questo building 
    def get_nodes(self):
        result = []
        for room in self.rooms:
            result.extend(room.nodes)
        return result
    
    ## 
    # @return Lista completa di sensor presenti in questo building 
    def get_sensors(self):
        result = []
        for room in self.rooms:
            result.extend(room.get_sensors())
        return result
    
    ## 
    # @return Lista completa di actuator presenti in questo building 
    def get_actuators(self):
        result = []
        for room in self.rooms:
            result.extend(room.get_actuators())
        return result    
    
    ## 
    # @return Lista completa di variable presenti in questo building 
    def get_variables(self):
        result = []
        for room in self.rooms:
            result.extend(room.get_variables())
        return result
    
    
    
    
    ##
    # @param room_id
    # @return La room con id pari a quello richiesto; None se non la trova
    def get_room(self, room_id):
        for room in self.rooms:
            if room.room_id == room_id:
                return room
        return None
    
    ##
    # @param node_id
    # @return il node con id pari a quello richiesto; None se non lo trova
    def get_node(self, node_id):
        for room in self.rooms:
            result = room.get_node(node_id)
            if result != None:               # Quando lo trova
                return result                # lo restituisce
        return None                          # Se non lo trova ritorna None
    
    ##
    # @param sensor_id
    # @return Il sensor con id pari a quello richiesto; None se non lo trova
    def get_sensor(self, sensor_id):
        for room in self.rooms:
            result = room.get_sensor(sensor_id)
            if result != None:               # Quando lo trova
                return result                # lo restituisce
        return None                          # Se non lo trova ritorna None
    
    ##
    # @param actuator_id
    # @return L'actuator con id pari a quello richiesto; None se non lo trova
    def get_actuator(self, actuator_id):
        for room in self.rooms:
            result = room.get_actuator(actuator_id)
            if result != None:               # Quando lo trova
                return result                # lo restituisce
        return None                          # Se non lo trova ritorna None
    
    ##
    # @param variable_id
    # @return La variable con id pari a quello richiesto; None se non la trova
    def get_variable(self, variable_id):
        for room in self.rooms:
            result = room.get_variable(variable_id)
            if result != None:               # Quando lo trova
                return result                # lo restituisce
        return None                          # Se non lo trova ritorna None
    
    
    
    