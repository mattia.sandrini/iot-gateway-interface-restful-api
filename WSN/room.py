

class Room():
    
    """latitude = None         # float
    longitude = None        # float
    room_id = None          # integer
    identifier = ''         # string
    intended_usage = ''     # string
    orientation = ''        # string
    floor = None            # integer
    width = None            # float
    length = None           # float
    height = None           # float
    volume = None           # float
    area = None             # float
    num_windows = None      # integer
    total_surface = None    # float
    glazing_surface = None  # float
    envelope_type = None    # float
    capacity = None         # integer
    ip_gateway = ''         # string
    abstract1 = None        # boolean
    room_code = ''          # string
    label = ''              # string"""
    
    room_id = None          # integer
    identifier = ''         # string
    ip_gateway = ''         # string
    
    nodes = []
    
    unique_identifier = ""
    
    
    def __init__(self):
        pass
    
    ## Costruisce l'identificativo univoco del locale, assemblando l'identificativo del locale stesso
    #  assieme all'identificativo univoco dell'edificio che lo contiene, e così via.
    #  Poi istruisce anche il resto della catena a fare lo stesso.
    #  @param parent identificatore univoco dell'edificio che contiene il locale considerato
    def built_unique_identifiers(self, parent):
        self.unique_identifier = parent +':'+ self.identifier  # Concateno l'identificatore univoco del genitore con l'identificatore dell'oggetto considerato
        for node in self.nodes:
            node.built_unique_identifiers(self.unique_identifier)
    
    
    ## 
    # @return Lista completa di sensor presenti in questa room 
    def get_sensors(self):
        result = []
        for node in self.nodes:
            result.extend(node.sensors)
        return result
    
    ## 
    # @return Lista completa di actuator presenti in questa room 
    def get_actuators(self):
        result = []
        for node in self.nodes:
            result.extend(node.actuators)
        return result
    
    ## 
    # @return Lista completa di variable presenti in questa room 
    def get_variables(self):
        result = []
        for node in self.nodes:
            result.extend(node.get_variables())
        return result
    
    
    
    ##
    # @param node_id
    # @return il node con id pari a quello richiesto; None se non lo trova
    def get_node(self, node_id):
        for node in self.nodes:
            if node.node_id == node_id:
                return node
        return None
    
    ##
    # @param sensor_id
    # @return Il sensor con id pari a quello richiesto; None se non lo trova
    def get_sensor(self, sensor_id):
        for node in self.nodes:
            result = node.get_sensor(sensor_id)
            if result != None:                     # Quando lo trova
                return result                      # lo restituisce
        return None                                # Se non lo trova ritorna None
    
    ##
    # @param actuator_id
    # @return L'actuator con id pari a quello richiesto; None se non lo trova
    def get_actuator(self, actuator_id):
        for node in self.nodes:
            result = node.get_sensor(actuator_id)
            if result != None:                     # Quando lo trova
                return result                      # lo restituisce
        return None                                # Se non lo trova ritorna None
    
    ##
    # @param variable_id
    # @return La variable con id pari a quello richiesto; None se non la trova
    def get_variable(self, variable_id):
        for node in self.nodes:
            result = node.get_variable(variable_id)
            if result != None:                      # Quando la trova
                return result                       # la restituisce
        return None                                 # Se non la trova ritorna None
    
    
    