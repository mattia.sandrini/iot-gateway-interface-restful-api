
class Actuator():
    
    """
    programmable = None # boolean
    controllable = None # boolean
    bookable = None     # boolean
    actuator_id = None  # integer
    identifier = ''     # string
    start_time = None   # time without time zone
    stop_time = None    # time without time zone
    operating_mode = '' # string
    accuracy = None     # float
    description = ''    # string
    responsetime = None # float
    """
    
    actuator_id = None  # integer
    identifier = ''     # string
    responsetime = None # float

    variables = []      # Lista delle variabili controllate da questo attuatore
    
    unique_identifier = ""
    
    
    def __init__(self):
        pass
    
    
    ## Costruisce l'identificativo univoco dell'attuatore, assemblano l'identificativo dell'attuatore stesso
    #  assieme all'identificativo univoco del nodo che lo contiene, e così via.
    #  Poi istruisce anche il resto della catena a fare lo stesso.
    def built_unique_identifiers(self, parent):
        self.unique_identifier = parent +':'+ self.identifier
        for variable in self.variables:
            variable.built_unique_identifiers(self.unique_identifier)
        
        
    ##
    # @param variable_id
    # @return La variable con id pari a quello richiesto; None se non la trova
    def get_variable(self, variable_id):
        for variable in self.variables:
            if variable.variable_id == variable_id:
                return variable
        return None     
        
        
        
        