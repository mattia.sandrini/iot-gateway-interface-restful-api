
## Questo modulo si occupa di richiedere lo stato dei sensori, e di eseguire azioni sui controllori, etc., attraverso le API/JSON di Domoticz. 


# URL:

# /json.htm?type=devices&rid=IDX     # Ottengo lo stato di un dispostivo

# /json.htm?type=devices&filter=all&used=true&order=Name   # Ottengo tutti i dispositivi di un certo tipo. { Filtri: light (lights/switches), weather, temp, utility}. (Order=Name è facoltativo)

# /json.htm?type=command&param=addlogmessage&message=MESSAGE   # Aggiunge un messaggio di og a domoticz

# /json.htm?type=command&param=switchlight&idx=99&switchcmd=On   # Accende/Spegne uno switch

# /json.htm?type=command&param=switchlight&idx=99&switchcmd=Toggle  # Commuta uno switch

# /json.htm?type=lightlog&idx=IDX    # Restituisce la lista di log di uno switch  (poco utile se vengono registrati su influxDB)
#
#
# Le seguenti sono escluse dalla documentazione delle Domoticz API:
#
# /json.htm?type=hardware&filter=all   (Restituisce i dati dei controller)
#
# /json.htm?type=openzwavenodes&idx=2   (Fornisce i dati di configurazione della z-stick e dei nodi della sua rete)
#                                        Ma mettendo l'idx dei nodi si ottiene errore.
#
#  Il sorgente che contiene il codice per gestire le seguenti richieste si trova al link: https://github.com/domoticz/domoticz/blob/master/www/zwavetopology.html
#
# /json.htm?type=command&param=zwavegroupinfo&idx=2   (Restituisce informazioni su eventuali gruppi Z-Wave)
# 
# http://localhost:8080/json.htm?type=command&param=zwavenetworkinfo&idx=2  (Fronisce i dettagli della rete mesh di sensori)
#
#
# Riassumendo:
#
# Hanno tutte il type:   
# 1) "type={devices|command}"
#
# 2) "type=devices" -> "rid=IDX" | "filter={all|light|weather|temp|utility}" & "used=true"
#
# 3) "type=command" -> "param=switchlight" & "idx=IDX" & "switchcmd={On|Off|Toggle}" 
#
# Attenzione: per  /json.htm?type=devices&rid=IDX   il formato del risultato cambia a seconda del dispositivo...
#             Per il termometro è: r.json()['result'][0]['Temp']     -> 26.5
#                                  r.json()['result'][0]['Humidity'] -> 43
#                                                        'Data'      -> "26.5 C, 43 %"
#                                                        'Type'      -> "Temp + Humidity"
#
#             Lo switch vale per smart plug, door sensor, prossimità, ecc...
#             Per lo switch è:     r.json()['result'][0]['Status'] -> "On"/"Off"
#                                                        'Data'    -> "On"/"Off"  (uguale a Status)
#                                                        'Type'    -> "Light/Switch"
#
#             Per il consumo è:  c=r.json()['result'][0]['Data']   -> "3.0 Watt" -> float(c.split(" ")[0]) -> 3.0
#                                                        'Type'    -> "Usage"
#
#             Per la luminosità: l=r.json()['result'][0]['Data']   -> "20 Lux"   -> int(l.split(" ")[0]) -> 20
#                                                        'Type'    -> "Lux"
#
#             Per i raggi UV:      r.json()['result'][0]['UVI']    -> 0.0
#                                                        'Data'    -> "0.0 UVI"
#                                                        'Type'    -> "UV"

import requests

host = "localhost"
port = 8080


## Restituisce la lista di dispositivi secondo un filtro di selezione (opzionale se si vuole la lista completa).
#  @param filter Effettua una selezione sul risultato in base al tipo di dispositivi che sono richiesti. filter={all|light|weather|temp|utility}
#  @param used Indica se si vogliono o meno ottenere solo i dispositivi utilizzati. used={True|False|None}
#  @return Struttura iterativa contenente una collezione di dispositivi, per ciascuno dei quali riporta le seguenti informazioni: Nome, Dato, Ultimo Aggiornamento, Tipo, Utilizzato, IDX
def get_devices_list(filter="all", used=None):
    
    param_used = '&used=true' if used else ('&used=false' if used==False else '')
    url = 'http://%s:%s/json.htm?type=devices&filter=%s%s' % (host, port, filter, param_used)  # Costruisco l'URL della richiesta
    r = requests.get(url)               # Invio la richiesta HTTP al servizio Domoticz

    for device in r.json()['result']:   # Per ogni dispositivo presente nella lista ottenuta in risposta alla richiesta apena effettuata
        yield { 'Name': device['Name'], 'Data': device['Data'], 'LastUpdate': device['LastUpdate'], 'Type': device['Type'], 'Used': device['Used'], 'idx': device['idx'] }


## Restituisce tutte le informazioni riguardanti il Device con IDX specificato
#  @param idx Identificatore interno di Domoticz assegnato al dispositivo interessato
#  @return JSON contenente le informazioni associate al dispositivo con IDX specificato
def get_device_info(idx):
    url = 'http://%s:%s/json.htm?type=devices&rid=%s' % (host, port, idx)
    r = requests.get(url)
    return r.json()['result'][0]


## Restituisce il dato (valore + eventuale unità di misura) dell'ultima misurazione effettuata dal Device con idx specificato.
#  Questi sono i formati delle informazioni esposte a seconda del tipo di dispositivo:
#    'Type': "Temp + Humidity" -> 'Data': "26.5 C, 43 %"
#    'Type': "Light/Switch"    -> 'Data': "On" | "Off"
#    'Type': "Usage"           -> 'Data': "3.0 Watt"
#    'Type': "Lux"             -> 'Data': "20 Lux"
#    'Type': "UV"              -> 'Data': "0.0 UVI"
#
#  @param idx Identificatore interno di Domoticz assegnato al dispositivo interessato
#  @return Ultimo dato (valore + eventuale unità di misura) registrato dal dispositivo specificato
def get_device_data(idx):
    info = get_device_info(idx)
    return info['Data']


## Restituisce l'ultimo valore assunto, o gli ultimi valori assunti, (senza unità di misura) dal dispositivo con idx specificato. Alcuni dispositivi (come il termometro) possono riportare 2 valori contemporaneamente
#  @param idx Identificatore interno di Domoticz assegnato al dispositivo interessato
#  @return Ultimo valore (o insieme di valori) registrato dal dispositivo specificato (corredato di timestamp (non UTC), e coppia "Nome misura":valore)
def get_device_values(idx):  
    info = get_device_info(idx)
    
    if info['Type'] == 'Temp + Humidity': 
        values = { "time": info['LastUpdate'], "Temp": info['Temp'], "Humidity": info['Humidity'] }
        
    elif info['Type'] == 'Light/Switch':
        values = { "time": info['LastUpdate'], "Status": info['Status'] }
        
    elif info['Type'] == 'Usage':
        values = { "time": info['LastUpdate'], "Power": float(info['Data'].split(" ")[0]) }
        
    elif info['Type'] == 'Lux':
        values = { "time": info['LastUpdate'], "Lux": int(info['Data'].split(" ")[0]) }
        
    elif info['Type'] == 'UV':
        values = { "time": info['LastUpdate'], "UVI": info['UVI'] }
        
    else:
        values = None
        
    return values
    
    
    
## Esegue un comando su un dispositivo attuatore di tipo switch On/Off
#  @param idx Identificativo di domoticz per il device su cui si vuole operare
#  @param new_status Lo stato che si vuole fargli assumere (0/1)
#  @return True se ha avuto successo, altrimenti False
def command_switch_light(idx, new_status):
    
    if type(new_status) is str:                         # Se è una stringa
        new_status = new_status.strip('"').strip("'")   # Rimuovo gli eventuali apici a inizio e fine stringa
        new_status = int(new_status)                    # COnverto in intero
    
    translate = {0: 'Off', 1: 'On'}
        
    r = requests.get('http://%s:%d/json.htm?type=command&param=switchlight&idx=%s&switchcmd=%s' % (host, port, idx, translate[new_status]))
    success = r.json()['status']     # Ritorna 'OK' se è andato tutto bene, altrimenti 'ERROR'
    
    return success == 'OK'     # Ritorna True se è andato tutto bene, altrimenti false








