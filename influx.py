
## Questo modulo si occupa di interrogare il database InfluxDB, e fornire i risultati delle query
#  sotto forma di liste di misure, o in formato JSON.

from influxdb import InfluxDBClient

influx_host = 'localhost'
influx_port = 8086
influx_user = 'root'
influx_pass = 'root'
influx_db = 'domoticz'

# NB:Il timestamp di InfluxDB è sempre in UTC

# Attenzione:  result.get_points() dovrebbe poter accettare anche un dict di tag 
#                                  su cui fare i controlli, ma non funziona.



## Scrive la lista di misure senza riportare il nome della measurement e i tag associati ad ogni misura
#  @param points Lista di misure, corredati di un campo time, value ed altri meta-dati, che devono essere convertite in formato JSON
#  @return Formato JSON della lista di misure in ingresso
def jsonify(points=[]):
    ris = '[ '    
    for p in points:    # Scandisco ogni punto della series
        ris += '{ "value": %d, "time": "%s" },' % (p['value'], p['time'])
    ris = ris[:len(ris)-1]    # Cancello l'ultima virgola, oppure lo spazio dopo la '[' se non c'erano punti
    ris += ' ]'
    return ris



## Data una lista di misure, fornisce solo quelle che soddisfano i tag richiesti (i campi valore dei tag coincidono con le coppie "tag":"valore_tag" presenti nel parametro tags)
#  @param points Lista di misure, corredati di un campo time, value ed altri meta-dati, su cui si vuole operare la selezione in base ai valori dei tag
#  @param tags Coppie nome:valore relative ai tag associati alle misure, sui quali si vuole operare la selezione
#  @return Lista di misure che soddisfano tutti i valori dei tag richiesti in congiunzione logica (AND)
def select_points_by_tags(points, tags={}):    
    result = []
    if tags != {}:
        for p in points:
            ok = True       # Se alla fine dei controlli in congiunzione logica sui tag, la variabile ok vale ancora True, allora la misura considerata verrà aggiunta al risultato
            for key in tags:                       # Ciclo per tutti i tag da controllare (in congiunzione logica: AND)
                if str(p[key]) != str(tags[key]):  # Se il valore associato ad un certo tag non coincide con quello richiesto (anche uno solo)
                    ok = False                     # La misura considerata non viene aggiunta al risultato
            if ok:
                result.append(p)
        return result
    else:
        return points


## Si connette all'host e database specificati nei parametri di configurazione del modulo, esegue la query in ingresso e ne fornisce il risultato come lista di misure
#  @param query Comando di query da eseguire
#  @return Lista di misure che soddisfano la query richiesta
def query(query):
    client = InfluxDBClient(influx_host, influx_port, influx_user, influx_pass, influx_db)
    result = client.query(query)
    points = result.get_points()  # Ottengo la serie di misure in una struttura iterativa (Generator)
    return list(points)           # Converto il generator in list


## Permette di filtrare le misure in base ai valori dei tag ad esse accociate, direttamente nella clausola WHERE della query
#  @param tags Coppie nome:valore che vengono fatte corrispondere ai tag associati alle singole misure
#  @return Stringa vuota '' se tags={}, altrimenti ritorna la clausola  "WHERE tag_name = '1' AND ..." che permette di fare la selezione sui valori specificati in tags
def generate_where_clause(tags={}):
    if tags != {}:               # Se è presente almeno un tag
        where_clause = "WHERE "  # Deve risultare una cosa come: WHERE tag_name = '1' AND ...
        for key in tags:
            where_clause += key + " = " + "'%s' AND " % str(tags[key])  # Anche se il valore del tag fosse un intero (p.e. identificativo numerico) va sempre e solo messo tra apici singoli
            
        return where_clause[:len(where_clause)-4]  # rimuovo la AND finale
    else:
        return ''


""" Ottiene tutte le misure dal database, poi ne fa la selezione richiamando la funzione  select_points_by_tags
    Dalle prove sui tempi di risposta risulta che utilizzando questo modo o utilizzando la clausola where, i tempi di risposta sono simili.

## Fornisce la lista di misure associate ad una specifica measurement, e che soddisfino (in congiunzione logica) i tag specificati
#  @param measurement Nome della tabella da cui ottenere le misure
#  @param tags Coppie nome:valore che vengono fatte corrispondere ai tag associati alle singole misure
#  @return Misure che appartengono alla measurement specificata, soddisfano (in congiunzione logica) i tag specificati, e sono ordinate in modo crescente nel tempo. Ritorna una lista vuota se non ci sono misure.
def get_measurement(measurement, tags={}):
    points = query("SELECT * FROM %s ORDER BY time ASC;" % measurement)  
    return select_points_by_tags(points, tags)         # Seleziono solo le misure che soddisfano i valori dei tag richiesti
"""

## Fornisce la lista di misure associate ad una specifica measurement, e che soddisfino (in congiunzione logica) i tag specificati
#  @param measurement Nome della tabella da cui ottenere le misure
#  @param tags Coppie nome:valore che vengono fatte corrispondere ai tag associati alle singole misure
#  @return Misure che appartengono alla measurement specificata, soddisfano (in congiunzione logica) i tag specificati, e sono ordinate in modo crescente nel tempo. Ritorna una lista vuota se non ci sono misure.
def get_measurement(measurement, tags={}):
    where_clause = generate_where_clause(tags)    # Selezionerà solo le misure che soddisfano i valori dei tag richiesti
    points = query("SELECT * FROM %s %s ORDER BY time ASC;" % (measurement, where_clause))  
    return points         

def get_measurement_json(measurement, tags={}):
    points = get_measurement(measurement, tags={})
    return jsonify(points)
    




""" Se sono presenti tante misure questo modo è troppo inefficiente (ottiene tutte le misure, seleziona in base ai tag specificati, e solo alla fine considera l'unico valore desiderato)

## Fornisce l'ultima misura registrata associata ad una specifica measurement, e che soddisfi (in congiunzione logica) i tag specificati
#  @param measurement Nome della tabella da cui ottenere la misura
#  @param tags Coppie nome:valore che vengono fatte corrispondere ai tag associati alle singole misure
#  @return Ultima misura registrata appartenente alla measurement specificata, che soddisfi (in congiunzione logica) i tag specificati. Ritorna una lista vuota se non ci sono misure.
def get_last_measure(measurement, tags={}):
    points = get_measurement(measurement, tags)  # Ottengo la lista completa delle misure ordinate in base al tempo, dalla più antica alla più recente
    if points != []:
        return points[len(points)-1]             # Ritorno l'ultima misura effettuata
    else:
        return []
"""


## Fornisce l'ultima misura registrata associata ad una specifica measurement, e che soddisfi (in congiunzione logica) i tag specificati
#  @param measurement Nome della tabella da cui ottenere la misura
#  @param tags Coppie nome:valore che vengono fatte corrispondere ai tag associati alle singole misure
#  @return Ultima misura registrata appartenente alla measurement specificata, che soddisfi (in congiunzione logica) i tag specificati. Ritorna una lista vuota se non ci sono misure.
def get_last_measure(measurement, tags={}):
    # Non posso usare la funzione select_points_by_tags sul risultato della query,
    # per cui devo filtrare i tag all'interno della query stessa
    
    where_clause = generate_where_clause(tags)
    points = query("SELECT * FROM %s %s ORDER BY time DESC LIMIT 1;" % (measurement, where_clause))
    
    if points != []:
        return points[0]   # Ritorno l'unico punto fornito dalla query
    else:
        return None
  
def get_last_measure_json(measurement, tags={}):
    point = get_last_measure(measurement, tags={})
    return jsonify(point)


""" Ottiene tutte le misure dal database, poi ne fa la selezione richiamando la funzione  select_points_by_tags
    Dalle prove sui tempi di risposta risulta che utilizzando questo modo o utilizzando la clausola where, i tempi di risposta sono simili.

## Fornisce la lista di misure associate ad una specifica measurement, che soddisfino (in congiunzione logica) i tag specificati, ed cadono in uno specifico intervallo temporale
#  Esempio: from_='2016-05-19T00:00:00.0Z', to='2016-05-19T01:00:00.0Z' -> Fornirà le misure registrate tra la mezzanotte e l'1:00 del giorno 19 maggio 2016.
#  @param measurement Nome della tabella da cui ottenere le misure
#  @param tags Coppie nome:valore che vengono fatte corrispondere ai tag associati alle singole misure
#  @param from_ Timestamp che segna l'inizio dell'intervallo temporale da considerare per fare la selezione delle misure
#  @param to Timestamp che segna la fine  dell'intervallo temporale da considerare per fare la selezione delle misure
#  @return Misure che appartengono alla measurement specificata, soddisfano (in congiunzione logica) i tag specificati, appartengono all'intervallo specificato, e sono ordinate in modo crescente nel tempo. Ritorna una lista vuota se non ci sono misure.
def get_measurement_by_interval(measurement, tags={}, from_='', to=''):
    
    if from_ == '' and to == '':                    # Se non è stato specificato alcun intervallo 
        return get_measurement(measurement, tags)   # Richiamo la funzione canonica
    elif from_ != '' and to == '':                  # Se è stato specificato solo l'inizio dell'intervallo
        where_clause = "WHERE time > '%s'" % from_
    elif from_ == '' and to != '':                  # Se è stata specificata solo la fine  dell'intervallo
        where_clause = "WHERE time < '%s'" % to
    else:                                           # Se è stato specificato un intervallo completo
        where_clause = "WHERE time > '%s' AND time < '%s'" % (from_, to)  
    
    points = query("SELECT * FROM %s %s ORDER BY time ASC;" % (measurement, where_clause))
    return select_points_by_tags(points, tags)
"""

## Fornisce la lista di misure associate ad una specifica measurement, che soddisfino (in congiunzione logica) i tag specificati, ed cadono in uno specifico intervallo temporale
#  Esempio: from_='2016-05-19T00:00:00.0Z', to='2016-05-19T01:00:00.0Z' -> Fornirà le misure registrate tra la mezzanotte e l'1:00 del giorno 19 maggio 2016.
#  @param measurement Nome della tabella da cui ottenere le misure
#  @param tags Coppie nome:valore che vengono fatte corrispondere ai tag associati alle singole misure
#  @param from_ Timestamp che segna l'inizio dell'intervallo temporale da considerare per fare la selezione delle misure
#  @param to Timestamp che segna la fine  dell'intervallo temporale da considerare per fare la selezione delle misure
#  @return Misure che appartengono alla measurement specificata, soddisfano (in congiunzione logica) i tag specificati, appartengono all'intervallo specificato, e sono ordinate in modo crescente nel tempo. Ritorna una lista vuota se non ci sono misure.
def get_measurement_by_interval(measurement, tags={}, from_='', to=''):
    
    where_clause = generate_where_clause(tags)
    
    if where_clause == '':       # Se non si è filtrato su nessun tag, la where_clause è una stringa vuota
        where_clause = "WHERE "  # La inizializzo
    else:
        where_clause += " AND "  # Altrimenti aggiungo una AND per appendere in coda le condizioni sugli eventuali intervalli temporali
    
    if from_ == '' and to == '':                    # Se non è stato specificato alcun intervallo 
        return get_measurement(measurement, tags)   # Richiamo la funzione canonica (la where_clause generata non serve a nulla in questo caso)
    elif from_ != '' and to == '':                  # Se è stato specificato solo l'inizio dell'intervallo
        where_clause += "time > '%s'" % from_
    elif from_ == '' and to != '':                  # Se è stata specificata solo la fine  dell'intervallo
        where_clause += "time < '%s'" % to
    else:                                           # Se è stato specificato un intervallo completo
        where_clause += "time > '%s' AND time < '%s'" % (from_, to)  
    
    points = query("SELECT * FROM %s %s ORDER BY time ASC;" % (measurement, where_clause))
    return points


def get_measurement_by_interval_json(measurement, tags={}, from_='', to=''):
    point = get_measurement_by_interval(measurement, tags, from_, to)
    return jsonify(point)








