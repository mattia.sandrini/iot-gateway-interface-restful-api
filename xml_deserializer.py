      
import xml.etree.ElementTree as ET


##
# Si occupa di importare i moduli necessari all'utilizzo delle classi durante la fase di creazione delgi oggetti letti dal file XML
# @param class_type Nome completo della classe, con la sequenza di package e moduli da cui deriva
def organize_import(class_type):
    module_sep = '.'                                  # Separatore
    modules = class_type.split(module_sep)            # Ottengo la sequenza annidata di moduli interni ai package
    modules = modules[:len(modules)-1]                # L'ultima voce della sequenza è il nome della classe, la devo eliminare
    import_cmd = 'import ' + module_sep.join(modules) # Ricongiungo i moduli
    exec(import_cmd, globals())                       # Eseguo l'istruzione di import, nell'ambiente di esecuzione globale


# ElementTree.Element.tag     # Fornisce il nome del tag oggetto di Element
# ElementTree.Element.attrib  # Fornisce un dizionario di attributi associati al tag oggetto di Element
# ElementTree.Element.text    # Fornisce il dato contenuto nel tag oggetto di Element



##
# Dato in ingresso un tag radice per una lista di tag, ne costruisce per ciascuno di essi l'oggetto istanza della classe contenuta nell'attributo 'class' del tag, lo aggiunge alla lista, e restituisce questa lista
# E' una procedura ricorsiva.
# @param root_list_tag Un tag radice per una lista di tag
# @return Lista di oggetti corrispendoti ai singoli tag del sotto-albero afferente al tag radice ricevuto in ingresso
def parse_list(root_list_tag):
    
    obj_list = []
    for item_tag in root_list_tag:  # Navigo tutti i tag del sotto-albero afferente alla radice considerata
        
        # Tratto il caso particolare in cui nella lista sia presente un dato di tipo builtin
        if (str(item_tag.tag) in ('int', 'float', 'bool', 'dict')):
            obj_list.append(eval(item_tag.text))
            continue
        elif (item_tag.tag == 'str'):       # Non posso applicare eval ad una stringa
            obj_list.append(item_tag.text)
            continue
        elif (item_tag.tag == 'list'):      # Non devo considerare le liste all'interno di una lista (generano errore)
            continue
        
        # Attenzione: Le 2 linee di codice seguenti necessitano di importare il package WSN!
        class_type = item_tag.attrib['class']  # Per il tag attualmente considerato ricavo il valore dell'attributo 'class'
        
        organize_import(class_type)            # Mi assicuro che venga importato il modulo contenente le classi necessarie
        
        obj = eval(class_type)()               # Istanzio un oggetto della classe appena ricavata

        for attr_item_tag in item_tag:   # Navigo tutti i tag che corrisponderanno agli attributi dell'oggetto obj appena istanziato
            attr = attr_item_tag.tag     # Nome del tag corrisponde a nome dell'attributo di obj
            value = attr_item_tag.text   # Il dato contenuto nel tag corrisponde al valore dell'attributo considerato
            if attr in dir(obj):         # Controllo se l'attributo è effettivamente un attributo della classe a cui appartiene l'oggetto, altrimenti lo ignoro
                attr_type = attr_item_tag.attrib['type']   # Ogni tag riporta anche l'informazione su quale sia il tipo di dato dell'attributo al quale il tag corrisponde 
                
                if attr_type == 'str':              # Se l'attributo è una stringa, il su valore non può essere computato da eval
                    evaluated_value = value
                elif attr_type != 'list':           # Se non è una lista
                    evaluated_value = eval(value)   # se è un numero riceverà tipo int, float, se è un dict assumerà la struttura di un dizionario, ecc...
                else:
                    evaluated_value = parse_list(attr_item_tag)  # Se invece è una lista, procedo ricorsivamente
                
                setattr(obj, attr, evaluated_value) # Scrivo nell'attributo di obj, attualmente considerato, il valore ottenuto sopra e propriamente dotato di tipo
            else:
                pass  # Dovrebbe avvisare che il file XML è malformato
                    
        obj_list.append(obj)  # Dopo aver riempito tutti gli attributi del nuovo oggetto, lo si aggiunge in coda alla lista, e si ricomincia il ciclo
    return obj_list


##
# Partendo dal file XML specificato, costruisce un albero XML, lo scansiona e genera la lista di oggetti corrispondenti
# @param file Nome del file da cui ottenere le informazioni
# @return Restituisce la lista contenente gli oggetti corrispondenti ai tag figli del nodo radice dell'albero XML
def deserialize(file='in.xml'):
    
    tree = ET.parse(file)   
    root = tree.getroot()   # Fornisce l'ElementTree.Element corrispondente al tag radice

    obj_list = parse_list(root)  # Si assume che il tag root possieda una lista di tag, quindi ne estrapola le informazioni e le mette in una lista di oggetti

    return obj_list




# Test
"""
obj_list = deserialize('test.xml')

for obj in obj_list:
    print(obj.rooms[0].ip_gateway) # print(obj.building_id)
    print(obj.rooms[1].ip_gateway) # print(obj.building_id)
    for node in obj.rooms[1].nodes:
        print(node.identifier)
        for sensor in node.sensors:
            print(sensor.identifier)

print(obj_list[0].dizionario['key1'])
print(obj_list[0].lista[3]['2'])      # Dovrebbe stampare 2... OK
print(obj_list[0].lista[5].room_id)
"""



""" Formato del file xml da parsare è il seguente:
<?xml version="1.0" ?>
<buildings>
    <!--File contenente tutti gli oggetti Building, e le relative informazioni.--><building building_id="1" class="WSN.building.Building">
        <building_year type="int">1995</building_year>
        <energy_class type="str">A++</energy_class>
        ...
        <rooms type="list">
            <room class="WSN.room.Room" room_id="101">
                <identifier type="str">M1</identifier>
                <room_id type="int">101</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="102">
                <identifier type="str">MLab</identifier>
                <room_id type="int">102</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="103">
                <identifier type="str">MTA</identifier>
                <room_id type="int">103</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
        </rooms>
        <building_id type="int">1</building_id>
        ...
    </building>
    <building building_id="2" class="WSN.building.Building">
        <building_year type="int">1995</building_year>
        <energy_class type="str">A++</energy_class>
        ...
        <rooms type="list">
            <room class="WSN.room.Room" room_id="104">
                <identifier type="str">M1</identifier>
                <room_id type="int">104</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="105">
                <identifier type="str">MLab</identifier>
                <room_id type="int">105</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="106">
                <identifier type="str">MTA</identifier>
                <room_id type="int">106</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
        </rooms>
        <building_id type="int">2</building_id>
        ...
    </building>
</buildings>
"""


