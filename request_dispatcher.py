
## Il modulo "request_dispatcher.py" si occupa di intercettare le richieste REST in arrivo sulla porta 80, dopo di che le discrimina e compie il comando corrispondente. 

import jsonifier

from flask import Flask
from flask import request
app = Flask(__name__)


# L'oggetto envelope serve per mantenere le informazioni associate alle risorse, e per facilitare le ricerche, ecc...

envelope = None   # Deve essere inizializzata dal modulo main

#from WSN import envelope
#envelope = envelope.Envelope()   # Oppure faccio tutto in questo modulo
#envelope.deserialize(file_xml)



# Risposte in caso di errore. Vengono utilizzate dagli errorhandler di Flask, ma anche dalle singole funzioni che ricevono le richieste
not_found_response = '{ "status": "ERR", "description": "URI not found." }'
bad_request_response = '{ "status": "ERR", "description": "errore in uno degli attributi obbligatori." }'
internal_error_response = '{ "status": "ERR", "description": "Internal error." }'


@app.errorhandler(404)
def not_found(e):
    return not_found_response, 404

@app.errorhandler(400)
def bad_request(e):
    return bad_request_response, 400

@app.errorhandler(500)
def internal_error(e):
    return internal_error_response, 500



## Restituisce le informazioni disponibili per tutti i building in anagrafica, senza entrare nel dettaglio delle rooms in esso contenute 
#
@app.route('/buildings/', methods=['GET'])
def get_buildings():
    return jsonifier.jsonify(envelope.buildings, enable_recursion=False)

## Restituisce le informazioni disponibili per tutti i building in anagrafica, e ricorsivamente anche quelle delle rooms, etc.
#
@app.route('/buildings/full', methods=['GET'])
def get_buildings_full():
    return jsonifier.jsonify(envelope.buildings, enable_recursion=True)


## Restituisce l’elenco dettagliato di tutti i locali
#
@app.route('/rooms', methods=['GET'])   
def get_rooms():
    return jsonifier.jsonify(envelope.get_all_rooms(), enable_recursion=False)

## Restituisce l’elenco dettagliato di tutti i locali, e ricorsivamente anche quelle dei nodes, etc.
#
@app.route('/rooms/full', methods=['GET'])   
def get_rooms_full():
    return jsonifier.jsonify(envelope.get_all_rooms(), enable_recursion=True)


## Restituisce l’elenco dettagliato di tutti i nodi presenti nel sistema
#
@app.route('/nodes', methods=['GET'])   
def get_nodes():
    return jsonifier.jsonify(envelope.get_all_nodes(), enable_recursion=False)

## Restituisce l’elenco dettagliato di tutti i locali, e ricorsivamente anche quelle dei sensors ed actuators, etc.
#
@app.route('/nodes/full', methods=['GET'])   
def get_nodes_full():
    return jsonifier.jsonify(envelope.get_all_nodes(), enable_recursion=True)


## Restituisce l’elenco dettagliato di tutti i sensori presenti nel sistema
#
@app.route('/sensors', methods=['GET'])   
def get_sensors():
    return jsonifier.jsonify(envelope.get_all_sensors(), enable_recursion=False)

## Restituisce l’elenco dettagliato di tutti i locali, e anche quelle delle variables da essi osservate
#
@app.route('/sensors/full', methods=['GET'])   
def get_sensors_full():
    return jsonifier.jsonify(envelope.get_all_sensors(), enable_recursion=True)


## Restituisce l’elenco dettagliato di tutti gli attuatori presenti nel sistema
#
@app.route('/actuators', methods=['GET'])   
def get_actuators():
    return jsonifier.jsonify(envelope.get_all_actuators(), enable_recursion=False)

## Restituisce l’elenco dettagliato di tutti i locali, e anche quelle delle variables da essi controllate
#
@app.route('/actuators/full', methods=['GET'])   
def get_actuators_full():
    return jsonifier.jsonify(envelope.get_all_actuators(), enable_recursion=True)







## Restituisce le informazioni disponibili per il building specificato
#
@app.route('/buildings/<building_id>/', methods=['GET'])   
def get_building(building_id):
    building = envelope.search_building(building_id)  # Applico la ricerca tra la lista di buildings
    if building == None:
        return not_found_response, 404
    else:
        return jsonifier.jsonify(building)


## Restituisce l’elenco dettagliato di tutti i locali presenti nell’edificio
#
@app.route('/buildings/<building_id>/rooms', methods=['GET'])   
def get_building_rooms(building_id):
    building = envelope.search_building(building_id)  # Applico la ricerca tra la lista di buildings
    if building == None:
        return not_found_response, 404
    else:
        return jsonifier.jsonify(building.rooms)



## Restituisce l’elenco dettagliato dei nodi associati al building
#
@app.route('/buildings/<building_id>/nodes', methods=['GET'])   
def get_building_sensors(building_id):
    building = envelope.search_building(building_id)
    if building == None:
        return not_found_response, 404
    else:
        nodes = building.get_nodes()
        return jsonifier.jsonify(nodes)



## Restituisce l’elenco dettagliato delle variabili associate al building
#
@app.route('/buildings/<building_id>/variables', methods=['GET'])   
def get_building_variables(building_id):
    building = envelope.search_building(building_id)
    if building == None:
        return not_found_response, 404
    else:
        variables = building.get_variables()
        return jsonifier.jsonify(variables)
    
    
## Restituisce l’elenco dettagliato delle variabili associate al building
#
@app.route('/rooms/<room_id>/variables', methods=['GET'])   
def get_room_variables(room_id):
    room = envelope.search_room(room_id)
    if room == None:
        return not_found_response, 404
    else:
        variables = room.get_variables()
        return jsonifier.jsonify(variables)
    
    



## Restituisce i dettagli del nodo specificato
#
@app.route('/nodes/<node_id>', methods=['GET'])   
def get_sensor(node_id):
    node = envelope.search_node(node_id)
    if node == None:
        return not_found_response, 404
    else:
        return jsonifier.jsonify(node)


## Restituisce l’elenco dei nodi nel locale con id specificato
#
@app.route('/nodes/room/<room_id>', methods=['GET'])   
def get_room_sensors(room_id):
    room = envelope.search_room(room_id)
    if room == None:
        return not_found_response, 404
    else:
        return jsonifier.jsonify(room.nodes)
    




## Restituisce l’elenco dettagliato di tutte le variabili presenti nel sistema
#
@app.route('/variables', methods=['GET'])   
def get_variables():
    return jsonifier.jsonify(envelope.get_all_variables())

## Restituisce le informazioni associate alla variable con id specificato
#
@app.route('/variables/<variable_id>', methods=['GET'])   
def get_variable(variable_id):
    variable = envelope.search_variable(variable_id)
    if variable == None:
        return not_found_response, 404
    else:
        return jsonifier.jsonify(variable)
    
    

## Permette di ottenere i valori associati ad una variabile, in maniera completa o secondo un intervallo temporale specifico.
#  Inoltre, permette di settare il valore della variabile di stato associata ad un dispositivo fisico.
#
#  Esempi per l'accesso in lettura o scrittura alla seguente risorse (variable/value):

#  curl  -X PUT   http://localhost/variables/<variable_id>/value/  -d value={1|0}
#  curl  -X PUT   http://localhost/variables/2/value/  -d value=1

#  curl  -X GET   http://localhost/variables/<variable_id>/value/{last | from;to}
#  curl  -X GET   http://localhost/variables/2/value/last
#  curl  -X GET   http://localhost/variables/2/value/                # Se non viene specificato l'intervallo ritorna tutta la serie di misure

#  curl  -X GET   http://localhost/variables/2/value/2016-05-19T00:00:00.0Z;2016-05-19T00:10:00.0Z  # Specifica l'intervallo completo
#  curl  -X GET   http://localhost/variables/2/value/2016-05-19T00:00:00.0Z;                        # Specifica solo l'inizio dell'intervallo
#  curl  -X GET   http://localhost/variables/2/value/;2016-05-19T00:10:00.0Z                        # Specifica solo la fine  dell'intervallo
#
@app.route('/variables/<variable_id>/value/', methods=['GET', 'PUT']) 
@app.route('/variables/<variable_id>/value/<interval>', methods=['GET'])        # l'intervallo è opzionale
def variable_value(variable_id, interval=''):
    variable = envelope.search_variable(variable_id)
    if variable == None:
        return not_found_response, 404
    else:
        if request.method == 'GET':
            result = ''
            if interval == '':                         # Se non è stato specificato alcun intervallo
                result = variable.get_values() 
            elif interval == 'last':                   # Se si vuole unicamente l'ultima misura
                result = variable.get_last_value()
            else:                                      # Se è stato specificato un intervallo
                interval_bounds = interval.split(';')  # Gli estremi dell'intervallo sono separati da ';'
                if len(interval_bounds) == 1:          # Se è stato specificato solo l'inizio dell'intervallo
                    interval_bounds.append('')         # aggiungo come fine dell'intervalo un valore indefinito
                result = variable.get_values(from_=interval_bounds[0], to=interval_bounds[1])  
            return jsonifier.jsonify(result)
        
        elif request.method == 'PUT':
            new_value = request.values.get('value')    # Ottengo il valore inserito nel campo DATA della richiesta
            success = variable.set_value(new_value)    # Invio il comando all'attuatore
            if success:
                return 'OK'
            else:
                return 'ERROR', 500   # Internal Error per dire che la risorsa esiste, ma il web service non è riuscito a compiere su di essa l'operazione richiesta.

        else:
            return bad_request_response, 400





