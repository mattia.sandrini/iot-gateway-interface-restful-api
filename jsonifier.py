
##
# Con la funziona jsonify(data) è possibile ottenere il formato JSON di un dato 
# di tipo builtin (numerico, boolean, string, dizionario, mentre le liste sono considerate a parte),
# di tipo classe generica, o lista di dati non necessariamente omogenei.
# Non pone quasi limiti alla complessità della strutturazione di liste e classi,
# le classi possono ereditare, possono contenere tra gli attributi delle liste eterogenee di oggetti,
# e anche attributi oggetti istanza di classi generiche.
# I campi con valore Nullo (None) vengono comunque inseriti nel JSON, riportando il valore "None",
# probabilmente sarebbe meglio ignorarli.

# Risulta valido secondo le RFC 4627, RFC 7159 ed ECMA-404, validato sul sito: https://jsonformatter.curiousconcept.com/

# Casi particolari:
# - jsonify(None) -> { "type": "NoneType", "value": "None" }
# - jsonify([])   -> []



## Non considera builtin le liste, perché vengono trattate in un altro modo dalla funzione jsonify()
# @param data Dato sul quale si effetua il controllo di tipo, se appartiene ai tipi builtin
# @return Se l'input è di tipo builtin ritorna: True, altrimenti ritorna: False
def is_builtin(data):
    return type(data) in (int, float, bool, dict, str) or data == None


## Se il dato in ingresso è di tipo builtin, ne fornisce una stringa che può essere aggiunta 
#  come valore di un campo JSON secondo le specifiche (utilizza apici doppi, ...)
#  @param data dato di cui si vuole formattare il valore
#  @return Stringa formattata
def format_value(data):
    if type(data) in (str, bool) or data == None:
        result = '"%s"' % data   # Se è una stringa/boolean o valore None devo aggiungere gli apici
    elif type(data) is dict:     # Devo formattare il dizionario secondo le specifiche del JSON, cioè utilizzando gli apici doppi
        result = '{ '
        for key in data:
            result += '"%s": %s, ' % (key, format_value(data[key])) # Procedo ricorsivamente sul valore del campo del dizionario, che potrebbe essere un'altro dizionario
        result = result[:len(result)-2]
        result += ' }'
    else:
        result = str(data)
    return result


##
# @param data Il dato di cui viene generato il JSON 
# @return JSON del dato in ingresso, costruito secondo il modello: '{ "type": "tipo", "value": valore }'
def jsonify_builtin(data):
    # E' meglio se non tengo traccia del tipo di dato, in questo modo alleggerisco il JSON
    #return '{ "type": "%s", "value": %s }' % (type(data).__name__, format_value(data))
    return format_value(data)


## Funzione ricorsiva che fornisce il formato JSON del contenuto di una lista e delle liste al suo interno
# @parma obj_list Lista di oggetti non necessariamente omogenei (può contenere istanze di classi generiche e/o tipi builtin)
# @return JSON della lista in ingresso, costruito secondo il modello: '[ { "attr1": val1, ... }, { "attr1": val1, ... } ]
def jsonify_list(obj_list, enable_recursion=False):
    
    if len(obj_list) == 0:  # Se la lista è vuota
        return '[]'
    
    result = '[ '
    for item in obj_list:
        result += jsonify(item, enable_recursion)      # Potrebbe a sua volta essere una lista di oggetti, devo richiamare il metodo generico
        result += ', '    
    result = result[:len(result)-2]  # Devo eliminare gli ultimi 2 caratteri, cioè: ', '
    result += ' ]'
    
    return result 



##
# @param obj Oggetto istanza di una classe generica, di cui viene fornito il JSON
# @param enable_recursion Se vale True allora entra ricorsivamente negli elementi figli e ritorna le informazioni complessive, altrimenti si ferma ai soli attributi dell'oggetto o lista passata.
# @return JSON dell'oggetto in ingresso, costruito secondo il modello: '{ "attr1": val1, "attr2": [ { ... }, { ... }, ... ], "attr3": value3, ... }
def jsonify_obj(obj, enable_recursion=False):

    result = '{ '
    for attr in dir(obj):               # Per ogni attributo presente nella classe dell'oggetto obj
        if not attr.startswith("__"):   # Ignoro gli attributi speciali (come __init__, ecc...)
            value = getattr(obj, attr)  # Ottengo il valore dell'attributo attualmente considerato
            if not callable(value):     # Ignoro anche gli attribui che sono funzioni

                if type(value) is list:                       # Se l'attributo è una lista
                    if enable_recursion:                      # e se è stato richiesto di entrare nei dettalgi della gerarchia di contenimento sottostante
                        formatted_value = jsonify_list(value, enable_recursion) # ne ottengo il JSON
                    else:
                        continue                              # Altrimenti ignoro l'attributo lista in considerazione
                elif is_builtin(value) or value == None:
                    formatted_value = format_value(value)
                else:
                    formatted_value = jsonify(value)  # E' un oggetto istanza di una classe non builtin
                
                result += '"%s": %s, ' % (attr, formatted_value)
    result = result[:len(result)-2]  # Devo eliminare gli ultimi 2 caratteri, cioè: ', '
    result += ' }'
    return result 


## 
# Fornisce una stringa in formato JSON contenente i dati presenti nell'oggetto, o lista di oggetti, in input
# @param data E' il dato che viene ritornato in formato JSON, può essere un oggetto istanza di classi generiche (anche tipi builtin), o una lista di oggetti
# @param enable_recursion Se vale True allora entra ricorsivamente negli elementi figli e ritorna le informazioni complessive, altrimenti si ferma ai soli attributi dell'oggetto o lista passata.
# @return Stringa in formato JSON, in cui ogni campo corrisponde ad un attributo del singolo oggetto
def jsonify(data, enable_recursion=False):

    if type(data) is list:                          # Controllo se il dato fornito è una lista
        return jsonify_list(data, enable_recursion) # applico la funzione jsonify_list
    elif is_builtin(data):                          # Tratto il caso particolare in cui il dato sia di tipo builtin
        return jsonify_builtin(data)                # applico la funzione jsonify_builtin
    else:                                           # Altrimenti
        return jsonify_obj(data, enable_recursion)  # applico la funzione jsonify_obj







# Test
"""
from WSN import building, actuator, room
import WSN

b = building.Building()
b.rooms = [room.Room()]

print(jsonify([b, b, b]))
print(jsonify(3.14))
print(jsonify('stringa'))
print(jsonify_list([1, 3.14, "stringa", [1,3.14, {"a":1,"b":2}], WSN.sensor.Sensor()]))


print(format_value({'a':3.1, 'b': False, 'c': None}))

print(jsonify([1, 3.14, "stringa", [1,3.14, {"a":1,"b":2}], WSN.building.Building()]))

print(jsonify(None))
print(jsonify([]))

print(jsonify([b], True))
"""






