
# Le URI riconoscute da questo modulo sono state prese dal modello di EIS.

from flask import Flask
from flask import request
app = Flask(__name__)


@app.errorhandler(404)
def not_found(e):
    return "404 not found – errore di invocazione."

@app.errorhandler(400)
def bad_request(e):
    return "400 bad request – errore in uno degli attributi obbligatori."

@app.errorhandler(500)
def internal_error(e):
    return "500 internal error."


# Vedi p. 24 per gli user


@app.route('/buildings', methods=['GET'])   
def get_buildings():
    """ Restituisce le informazioni disponibili per tutti i building in anagrafica """
    pass

@app.route('/buildings/<building_id>', methods=['GET'])   
def get_building(building_id):
    """ Restituisce le informazioni disponibili per il building specificato """
    pass

@app.route('/buildings/district/<district_id>', methods=['GET'])   
def get_buildings_district(district_id):
    """ Restituisce le informazioni disponibili per tutti i building del distretto specificato """
    pass

@app.route('/buildings/<building_id>/sensors', methods=['GET'])   
def get_building_sensors(building_id):
    """ Restituisce l’elenco dettagliato dei sensori associati al building """
    pass

@app.route('/buildings/<building_id>/variables', methods=['GET'])   
def get_building_variables(building_id):
    """ Restituisce l’elenco dettagliato delle variabili associate al building """
    pass

@app.route('/buildings/<building_id>/plugs', methods=['GET'])   
def buildings(building_id):
    """ Restituisce l’elenco delle plug associate al building su cui è possibile effettuare una prenotazione """
    pass

@app.route('/comfortems/single', methods=['POST'])   
def set_comfortems_single():
    """ Inserisce la scelta dell’utente sul comfort dell’ems """
    pass

@app.route('/comfortems/ems/<ems_id>', methods=['GET'])   
def get_comfortems_ems(ems_id):
    """ Restituisce l’ultimo comfort inserito dall’utente per l’ems specificato """
    pass

# /comfortfeedbacks/app/building/<building_id>/<year>/<month>/<day>?var=variable
@app.route('/comfortfeedbacks/app/building/<building_id>/<year>/<month>/<day>', methods=['GET'])   
def get_comfortfeedbacks(building_id, year, month, day):
    """ Restituisce per il giorno specificato e l’edificio specificato, per la variabile di comfort specificata – come distribuzione percentuale sulle fasce di comfort previste """
    var = request.args.get('var')
    pass

# Fino a pagina 7 specifiche.

# Ricomincio da pagina 36.

@app.route('/rooms', methods=['GET'])   
def get_rooms():
    """ Restituisce l’elenco dettagliato di tutti i locali """
    pass

@app.route('/rooms/district/<district_id>', methods=['GET'])   
def get_district_rooms(district_id):
    """ Restituisce l’elenco dettagliato di tutti i locali presenti nel distretto """
    pass

@app.route('/rooms/building/<building_id>', methods=['GET'])   
def get_building_rooms(building_id):
    """ Restituisce l’elenco dettagliato di tutti i locali presenti nell’edificio """
    pass

@app.route('/rooms/<room_id>/details', methods=['GET'])   
def get_room_details(room_id):
    """ Restituisce i dettagli associati al locale """
    pass

@app.route('/rooms/<room_id>/active', methods=['GET'])   
def is_room_active(room_id):
    """ Restituisce true se il locale contiene sensori adb """
    pass

@app.route('/rooms/<room_id>/active/list', methods=['GET'])   
def get_room_active_list(room_id):
    """ Restituisce l’elenco delle variabili monitorare da sensori adb nel locale """
    pass

# /rooms/roomcode?code=rcode
@app.route('/rooms/roomcode', methods=['GET'])   
def get_roomid():
    """ Restituisce il roomid relativo al locale identificato con roomcode specificato
        Nota: solo i locali inseriti nella sperimentazione hanno un roomid associato """
    code = request.args.get('code')
    pass

@app.route('/sensors/<sensor_id>', methods=['GET'])   
def get_sensor(sensor_id):
    """ Restituisce i dettagli del sensore specificato """
    pass

@app.route('/sensors/room/<room_id>', methods=['GET'])   
def get_room_sensors(room_id):
    """ Restituisce l’elenco dei sensori nel locale con id specificato """
    pass

@app.route('/sensorsettings/dom/single', methods=['POST'])   
def set_sensor_settings():
    """ Inserisce un intervallo di attività per il sensore specificato
        Nota: record singoli
        Nota: orario sul corrente fuso orario CET """
    pass

# Interrotto a pag. 40

# Riprende a pag. 44

@app.route('/variables', methods=['GET'])   
def get_variables():
    """ Restituisce l’elenco di tutte le variabili monitorate """
    pass

@app.route('/variables/<variable_id>', methods=['GET'])   
def get_variable(variable_id):
    """ Restituisce il dettaglio della variabile specificata """
    pass

@app.route('/variables/room/<room_id>', methods=['GET'])   
def get_room_variables(room_id):
    """ Errore nelle specifiche! """
    pass

@app.route('/variables/sensor/<sensor_id>/list', methods=['GET'])   
def get_sensor_variables(sensor_id):
    """ Restituisce il nome e l’identificativo delle variabili monitorate dal sensore specificato   """
    pass

@app.route('/measurements/variable/<variable_id>/instant', methods=['GET'])   
def get_variable_last_measure():
    """ Restituisce l’ultima misurazione disponibile per la variabile specificata """
    pass

# Nota: Le URI per le medie su 60 min e su 15 min sono del tutto analoghe, 
#       si potrebbe generalizzare e fornire anche altre medie...

@app.route('/measurements/60min/sensor/variable/<variable_id>/<year>/<month>/<day>', methods=['GET'])   
def get_variable_measures_mean_60_by_day(variable_id, years, month, day):
    """ Restituisce la media calcolata su 60 minuti delle rilevazioni per la variabile specificata nel giorno specificato """
    pass

# /measurements/60min/sensor/variable/<variable_id>/<year>/<month>/<day>?from={HH:MM}&to={HH:MM}
@app.route('/measurements/60min/sensor/variable/<variable_id>/<year>/<month>/<day>', methods=['GET'])   
def get_variable_measures_mean_60_by_interval(variable_id, years, month, day):
    """ Restituisce la media calcolata su 60 minuti delle rilevazioni per la variabile specificata nell’intervallo orario specificato della data specificata """
    from_ = request.args.get('from')
    to = request.args.get('to')
    pass

# /measurements/60min/sensor/variable/<variable_id>/<year>/<month>/<day>?weekly=true
@app.route('/measurements/60min/sensor/variable/<variable_id>/<year>/<month>/<day>', methods=['GET'])   
def get_variable_measures_mean_60_by_week(variable_id, years, month, day):
    """ Restituisce la media calcolata su 60 minuti delle rilevazioni per la variabile specificata nei 7 giorni precedenti rispetto alla data specificata """
    weekly = request.args.get('weekly')
    pass

@app.route('/measurements/60min/sensor/variable/<variable_id>/<year>/<month>', methods=['GET'])   
def get_variable_measures_mean_60_by_month(variable_id, years, month):
    """ Restituisce la media calcolata su 60 minuti delle rilevazioni per la variabile specificata nel mese specificato """
    pass

@app.route('/measurements/60min/sensor/variable/<variable_id>/<year>', methods=['GET'])   
def get_variable_measures_mean_60_by_year(variable_id, years):
    """ Restituisce la media calcolata su 60 minuti delle rilevazioni per la variabile specificata nell'anno specificato """
    pass

@app.route('/measurements/15min/sensor/variable/<variable_id>/<year>/<month>/<day>', methods=['GET'])   
def get_variable_measures_mean_15_by_day(variable_id, years, month, day):
    """ Restituisce la media calcolata su 15 minuti delle rilevazioni per la variabile specificata nel giorno specificato """
    pass

# /measurements/15min/sensor/variable/<variable_id>/<year>/<month>/<day>?from={HH:MM}&to={HH:MM}
@app.route('/measurements/15min/sensor/variable/<variable_id>/<year>/<month>/<day>', methods=['GET'])   
def get_variable_measures_mean_15_by_interval(variable_id, years, month, day):
    """ Restituisce la media calcolata su 15 minuti delle rilevazioni per la variabile specificata nell’intervallo orario specificato della data specificata """
    from_ = request.args.get('from')
    to = request.args.get('to')
    pass

# /measurements/15min/sensor/variable/<variable_id>/<year>/<month>/<day>?weekly=true
@app.route('/measurements/15min/sensor/variable/<variable_id>/<year>/<month>/<day>', methods=['GET'])   
def get_variable_measures_mean_15_by_week(variable_id, years, month, day):
    """ Restituisce la media calcolata su 15 minuti delle rilevazioni per la variabile specificata nei 7 giorni precedenti rispetto alla data specificata """
    weekly = request.args.get('weekly')
    pass

@app.route('/measurements/15min/sensor/variable/<variable_id>/<year>/<month>', methods=['GET'])   
def get_variable_measures_mean_15_by_month(variable_id, years, month):
    """ Restituisce la media calcolata su 15 minuti delle rilevazioni per la variabile specificata nel mese specificato """
    pass

@app.route('/measurements/15min/sensor/variable/<variable_id>/<year>', methods=['GET'])   
def get_variable_measures_mean_15_by_year(variable_id, years):
    """ Restituisce la media calcolata su 15 minuti delle rilevazioni per la variabile specificata nell'anno specificato """
    pass

# Interrotto a pag. 51

# Riprende a pag. 59

@app.route('/measurements/plug/<plug_id>', methods=['GET'])   
def get_plug_measurement(plug_id):
    """ Energia erogata dalla plug nell’ultima rilevazione disponibile """
    pass

@app.route('/measurements/list', methods=['POST'])   
def set_measurement_list():
    """ Inserisce un rilevazione effettuata su una variabile
        Nota: record multipli
        Nota: orario sul corrente fuso orario CET """
    pass

@app.route('/measurements/single', methods=['POST'])   
def set_measurement_single():
    """ Inserisce un rilevazione effettuata su una variabile
        Nota: record singolo
        Nota: orario sul corrente fuso orario CET """
    pass

# Interrotto a pag. 60











