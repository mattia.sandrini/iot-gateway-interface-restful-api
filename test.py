
## Questo modulo si occupa di creare una struttura di esempio, che viene poi serializzata su un file in formato XML.


from WSN import envelope, building, room, node, sensor, actuator, variable 

file_xml = 'file.xml'


b1 = building.Building()
b1.building_id = 1           
b1.identifier = "Modulo"         
b1.num_rooms = 5       

# Room 1
    
r1 = room.Room()
r1.room_id = 1
r1.identifier = "M1"
r1.ip_gateway = "192.168.1.101"
n1 = node.Node()
n1.node_id = 1      
n1.identifier = 'nodo_1'     
n1.labe = 'AN158_Everspring'
n1.indoor = True       
s1 = sensor.Sensor()
s1.sensor_id = 1    
s1.identifier = 'power_meter'   

v1 = variable.Variable()
v1.variable_id = 1
v1.measurement = 'usage'
v1.description = 'Consumo energetico'
v1.controllable = False
v1.name = 'power_meter'
v1.identifier = 'usage'
v1.unit = 'W'
v1.domoticz_device_idx = 16

s1.variables = [v1]
a1 = actuator.Actuator()
a1.actuator_id = 1  
a1.identifier = 'status_plug'     
a1.responsetime = 100

v2 = variable.Variable()
v2.variable_id = 2
v2.measurement = 'status'
v2.description = "Stato dell'interruttore On/Off"
v2.controllable = True
v2.name = 'status_switch'
v2.unit = ''
v2.identifier = 'status'
v2.domoticz_device_idx = 2

a1.variables = [v2] 
n1.sensors = [s1]
n1.actuators = [a1]

n2 = node.Node()
n2.node_id = 2      
n2.identifier = 'nodo_2'     
n2.indoor = True     
n2.labe = 'Aeotec Multisensor'  
s2 = sensor.Sensor()
s2.sensor_id = 2    
s2.identifier = 'temp_hum'   

v3 = variable.Variable()
v3.variable_id = 3
v3.measurement = 'temperature'
v3.description = "Misurazione della temperatura"
v3.controllable = False
v3.name = 'temperature'
v3.unit = 'C'
v3.identifier = 'temperature'
v3.domoticz_device_idx = 9
v4 = variable.Variable()
v4.variable_id = 4
v4.measurement = 'humidity'
v4.description = "Misurazione dell'umidità relativa"
v4.controllable = False
v4.name = 'humidity'
v4.unit = '%RH'
v4.identifier = 'humidity'
v4.domoticz_device_idx = 9

s2.variables = [v3, v4]

n2.sensors = [s2]
n2.actuators = []

n3 = node.Node()
n3.node_id = 3     
n3.identifier = 'nodo_3'     
n3.labe = 'Door/Window Sensor'
n3.indoor = True    

s3 = sensor.Sensor()
s3.sensor_id = 3  
s3.identifier = 'status_door'   

v5 = variable.Variable()
v5.variable_id = 5
v5.measurement = 'status'
v5.description = "Stato di apertura della porta"
v5.controllable = False
v5.name = 'status_door'
v5.unit = ''
v5.identifier = 'status'
v5.domoticz_device_idx = 1

s3.variables = [v5]

n3.sensors = [s3]
n3.actuators = []

r1.nodes = [n1, n2, n3]      



# Room 2

r2 = room.Room()
r2.room_id = 2
r2.identifier = "MLab1"
r2.ip_gateway = "192.168.1.101"



n4 = node.Node()
n4.node_id = 4      
n4.identifier = 'nodo_4'     
n4.indoor = True     
n4.labe = 'Aeotec Multisensor'  
s4 = sensor.Sensor()
s4.sensor_id = 4    
s4.identifier = 'luminosity'   

v6 = variable.Variable()
v6.variable_id = 6
v6.measurement = 'luminosity'
v6.description = "Misurazione del flusso luminoso"
v6.controllable = False
v6.name = 'luminosity'
v6.unit = 'lm'
v6.identifier = 'luminosity'
v6.domoticz_device_idx = 10
s4.variables = [v6]
n4.sensors = [s4]

n5 = node.Node()
n5.node_id = 5     
n5.identifier = 'nodo_5'     
n5.labe = 'Door/Window Sensor'
n5.indoor = True    

s5 = sensor.Sensor()
s5.sensor_id = 5  
s5.identifier = 'status_door'   

v7 = variable.Variable()
v7.variable_id = 7
v7.measurement = 'status'
v7.description = "Stato di apertura della porta"
v7.controllable = False
v7.name = 'status_door'
v7.unit = ''
v7.identifier = 'status'
v7.domoticz_device_idx = 4

s5.variables = [v7]

n5.sensors = [s5]
n5.actuators = []

r2.nodes = [n4, n5]

b1.rooms = [r1, r2]
building_list = [b1]


envelope = envelope.Envelope(building_list)   # Istanzio l'oggetto della classe Envelope che contiene la lista di building appena creata
envelope.built_unique_identifiers()           # Richiamo il metodo per la creazione automatica della catena di identificatori univoci
envelope.serialize(file_xml)                  # Serializzo la struttura sul file XML

