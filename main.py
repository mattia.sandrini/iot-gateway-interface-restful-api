#!/usr/bin/env python3

## Modulo principale
#  Deve essere eseguito con i privilegi di root, perché è necessario avere 
#  i privilegi di root per poter "bindare" la porta 80.

import request_dispatcher
app = request_dispatcher.app

file_xml = 'file.xml'

import test    # Importa ed esegue il codice di test per la generazione del file xml di prova      

from WSN import envelope

envelope = envelope.Envelope([])
envelope.deserialize(file_xml)          # Carico la struttura presente nel file XML

request_dispatcher.envelope = envelope  # Fornisco l'insieme delle risorse al modulo che dovrà soddisfare le richieste dei client


    
    
    
######################################## Entry Point
if __name__ == '__main__':             #
    app.run(host='0.0.0.0', port=80)   # 0.0.0.0 significa che sta in ascolto su tutti gli indirizzi IP della macchina
 
